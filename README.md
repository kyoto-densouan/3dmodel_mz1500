# README #  
  
1/3スケールのMZ-1500風小物のstlファイルです。  
スイッチ類、I/Oポート等は省略しています。  
  
組み合わせたファイルとパーツごとに配置したファイルの二種類があります。  
元ファイルはAUTODESK 123D DESIGNです。  


***

# 実機情報

## メーカ
- シャープ

## 発売時期
- MZ-1500 1984年6月1日

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/MZ-1500)
- [懐かしのパソコン](https://greendeepforest.com/?p=910)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_mz1500/raw/7280db36651166e100f4359d8fc72af59a7fc05d/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_mz1500/raw/7280db36651166e100f4359d8fc72af59a7fc05d/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_mz1500/raw/7280db36651166e100f4359d8fc72af59a7fc05d/ExampleImage.png)
